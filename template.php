<?php

/* Implements https://gist.github.com/pascalduez/1418121 */

/**
 * Implements hook_preprocess_html().
 */
function casc_dash_preprocess_html(&$vars) {
  // Move JS files "$scripts" to page bottom for perfs/logic.
  // Add JS files that *needs* to be loaded in the head in a new "$head_scripts" scope.
  // Reference the Gruntfile for a list of js scripts to be bundled.
  $path = drupal_get_path('theme', 'casc_dash');
  drupal_add_js($path . '/js/production.top.js', array('scope' => 'head_scripts', 'weight' => -1, 'preprocess' => FALSE));  
}

/**
 * Implements hook_process_html().
 */
function casc_dash_process_html(&$vars) {
  $vars['head_scripts'] = drupal_get_js('head_scripts');
}

/**
 * Implements hook_preprocess_page().
 */
function casc_dash_preprocess_page(&$vars) {
	// Convenience vars
  if (!empty($vars['page']['sidebar_first'])){
    $left = $vars['page']['sidebar_first'];
  }

  if (!empty($vars['page']['sidebar_second'])) {
    $right = $vars['page']['sidebar_second'];
  }

  // Dynamic sidebars
  if (!empty($left) && !empty($right)) {
    $vars['main_grid'] = 'large-6 push-3';
    $vars['sidebar_first_grid'] = 'large-3 pull-6';
    $vars['sidebar_sec_grid'] = 'large-3';
  } elseif (empty($left) && !empty($right)) {
    $vars['main_grid'] = 'large-9';
    $vars['sidebar_first_grid'] = '';
    $vars['sidebar_sec_grid'] = 'large-3';
  } elseif (!empty($left) && empty($right)) {
    $vars['main_grid'] = 'large-9 push-3';
    $vars['sidebar_first_grid'] = 'large-3 pull-9';
    $vars['sidebar_sec_grid'] = '';
  } else {
    $vars['main_grid'] = 'large-12';
    $vars['sidebar_first_grid'] = '';
    $vars['sidebar_sec_grid'] = '';
  }
}

