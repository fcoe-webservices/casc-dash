<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!--  HEADER  -->

  <header id="header">
    <div class="container row">
      <?php if ($logo): ?>
	      <div class="large-2 columns"
	        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
	          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
	        </a>
	      </div>
      <?php endif; ?>

      <?php if ($site_name || $site_slogan): ?>
        <div id="name-and-slogan"  class="large-10 columns">

          <?php if ($site_name): ?>
            <?php if ($title): ?>
              <div id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
              </div>
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
              </h1>
            <?php endif; ?>
          <?php endif; ?>

          <?php if ($site_slogan): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>

        </div>
      <?php endif; ?>

      <?php if ($page['header']): ?>
        <div id="header-region">
          <?php print render($page['header']); ?>
        </div>
      <?php endif; ?>
    </div>
  </header> <!-- /header -->

  <?php if ($main_menu || $secondary_menu): ?>
    <nav id="navigation" class="menu <?php if (!empty($main_menu)) {print "with-primary";}
      if (!empty($secondary_menu)) {print " with-secondary";} ?>">
      <div class="container row">
        <?php print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('links', 'clearfix', 'main-menu', 'inline-list')))); ?>
        <?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary', 'class' => array('links', 'clearfix', 'sub-menu')))); ?>
      </div>
    </nav> <!-- /navigation -->
  <?php endif; ?>

  <!--  MAIN  -->

  <div id="main" class="row">
    <div class="container">
    	<section id="content" class="<?php print $main_grid; ?> main columns">
    		<?php if ($title|| $messages || $tabs || $action_links): ?>
            <div id="content-header">

              <?php if ($page['highlighted']): ?>
                <div id="highlighted"><?php print render($page['highlighted']) ?></div>
              <?php endif; ?>

              <?php print render($title_prefix); ?>

              <?php if ($title): ?>
                <h1 class="title"><?php print $title; ?></h1>
              <?php endif; ?>

              <?php print render($title_suffix); ?>
              <?php print $messages; ?>
              <?php print render($page['help']); ?>


              <?php if ($tabs): ?>
                <div class="tabs"><?php print render($tabs); ?></div>
              <?php endif; ?>



              <?php if ($action_links): ?>
                <ul class="action-links"><?php print render($action_links); ?></ul>
              <?php endif; ?>

            </div> <!-- /#content-header -->
          <?php endif; ?>      

          <div id="content-area">
            <?php print render($page['content']) ?>
          </div>

        </section> <!-- /content-inner /content -->

        <?php if (!empty($page['dash_first'])): ?>
	        <section class="dashboard">
		        <div id="dash-first" class="large-6 columns">
		          <?php print render($page['dash_first']); ?>
		        </div>
	    		<?php endif; ?> 

		      <?php if (!empty($page['dash_second'])): ?>
		        <div id="dash-second" class="large-6 columns">
		          <?php print render($page['dash_second']); ?>
		        </div>
		      </section>
	      <?php endif; ?> 

       

      <?php if (!empty($page['sidebar_first'])): ?>
        <aside id="sidebar-first" class="<?php print $sidebar_first_grid; ?> columns sidebar">
          <?php print render($page['sidebar_first']); ?>
        </aside> <!-- /sidebar-first -->
      <?php endif; ?> 

      <?php if (!empty($page['sidebar_second'])): ?>
        <aside id="sidebar-second" class="<?php print $sidebar_sec_grid; ?> columns sidebar">
          <?php print render($page['sidebar_second']); ?>
        </aside> <!-- /sidebar-second -->
      <?php endif; ?>
    </div>
  </div> <!-- /main -->

  <!--  FOOTER  -->

  <?php if ($page['footer']): ?>
    <footer id="footer" class="row">
      <div class="container large-12 columns">
      <?php print render($page['footer']); ?>
      </div>
    </footer> <!-- /footer -->
  <?php endif; ?>

</div> <!-- /page -->
