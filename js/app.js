(function($) {

$(document).ready(function(){
  $(".toggler").click(function(){
  	$(this).find('.field-name-field-dops-evidence, .field-name-field-evidence').fadeToggle();
    return false;
  });
  $('.view-id-dops_reflections_page tr, .view-id-reflections tr').each(function() {
    //$(this).hide();
    var row = $(this);
    row.find('.question').first().appendTo(row.find('th'));
  });




  if ($(".page-node-add-dops-reflection, .page-user-dops-reflections, .node-type-dops-reflection, .page-user-reflections").length) {
			$(document).scroll(function() {
      var top = $(document).scrollTop();
      if (top > 300) $('.hidden').show();
      if (top < 300) $('.hidden').hide();
    	});
    	var div = $('.hidden');
    	var start = $(div).offset().top;
    $.event.add(window, "scroll", function() {
      var p = $(window).scrollTop();
      $(div).css('position',((p)>start) ? 'fixed' : 'static');
      $(div).css('top',((p)>start) ? '30px' : '');
  	});
  }

  $('.node-reflection-form .form-radio[value=_none]').parent().hide();

  $(".views-field-field-registration > .field-content > a").addClass("button small");

	});


}(jQuery));